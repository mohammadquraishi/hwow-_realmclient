﻿using System;
using System.Linq;
using Realms;
using Realms.Sync;
using System.Threading.Tasks;

namespace RealmClient
{
    class Program
    {
        private static int numberOfPatients;
        private static Random random = new Random();
        static void Main(string[] args)
        {
            numberOfPatients = int.Parse(args[0]);
            var serverURL = new Uri(String.Format("realm://{0}/~/patients", args[1]));
            var loginURL = new Uri(String.Format("http://{0}", args[1]));
            var username = args[2] ?? "admin@admin.com";
            var password = args[3] ?? "admin";
            var credentials = Credentials.UsernamePassword(username, password, false);
            var client = new RealmClient(args[4] ?? "default");
            var result = RunTest(client, credentials, loginURL, serverURL).Result;
        }

        static async Task<int> RunTest(RealmClient client, Credentials credentials, Uri loginURL, Uri serverURL)
        {
            await client.Test(credentials, loginURL, serverURL);
            return 0;
        }

        class RealmClient : IObserver<SyncProgress>
        {
            private DateTime _start;
            private DateTime _end;
            private const int MIN = 100;
            private const int MAX = 1000;
            private const int CHAR_LEN = 3;
            private readonly String _identifier;
            public String Identifier
            {
                get
                {
                    return _identifier;
                }
            }

            internal RealmClient(String identifier)
            {
                _identifier = identifier;
            }
            internal async Task Test(Credentials credentials, Uri loginURL, Uri serverURL)
            {
                var user = await User.LoginAsync(credentials, loginURL);
                var config = new SyncConfiguration(user, serverURL);
                var realm = Realm.GetInstance(config);
                var session = realm.GetSession();
                var token = session.GetProgressObservable(ProgressDirection.Download,
                    ProgressMode.ReportIndefinitely);
                token.Subscribe(this);
                Patient[] patients = new Patient[numberOfPatients];
                for (int i = 0; i < numberOfPatients; i++)
                {
                    patients[i] = new Patient()
                    {
                        firstName = RandomString(CHAR_LEN),
                        lastName = RandomString(CHAR_LEN),
                        mrn = random.Next(MIN, MAX).ToString()
                    };
                }

                realm.Write(() =>
                {
                    foreach (Patient p in patients)
                    {
                        realm.Add(p);
                    }
                });

            }

            void IObserver<SyncProgress>.OnCompleted()
            {
                var millis = (_end - _start).TotalMilliseconds;
                Console.WriteLine(String.Format("Time to sync: {0} on instance: {1}"), millis.ToString(), _identifier);
                Environment.Exit(0);
            }

            void IObserver<SyncProgress>.OnError(Exception error)
            {
                Console.WriteLine(error.StackTrace);
            }

            void IObserver<SyncProgress>.OnNext(SyncProgress value)
            {
                if(_start == null)
                {
                    _start = DateTime.Now;
                }
                if(value.TransferredBytes >= value.TransferableBytes)
                {
                    _end = DateTime.Now;
                }
            }

            String RandomString(int length)
            {
                const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                return new string(Enumerable.Repeat(chars, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }
        }
    }
}
