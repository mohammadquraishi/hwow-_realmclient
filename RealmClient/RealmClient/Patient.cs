﻿using System;
using Realms;

namespace RealmClient
{
    class Patient : RealmObject
    {
        public String firstName { get; set; }
        public String middleName { get; set; }
        public String lastName { get; set; }
        public String dob { get; set; }
        public String mrn { get; set; }
        public String hospitalId { get; set; }
        public String admitId { get; set; }
        public String admitDate { get; set; }
        [Ignored]
        public String fullName {
            get
            {
                return String.Format("{0} {1}", firstName, lastName);
            }
        }
    }
}
