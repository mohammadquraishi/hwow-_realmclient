﻿using System;
using Realms;

namespace RealmClient
{
    class Hospital : RealmObject
    {
        public String id { get; set; }
        public String hospitalName { get; set; }
        public String hospitalId { get; set; }
        public String streetAddress { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String phoneNumber { get; set; }
        [Ignored]
        public String mailingAddress
        {
            get
            {
                return String.Format("{0} {1} {2}", streetAddress, city, state);
            }
        }
    }
}
